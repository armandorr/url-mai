import numpy as np
from itertools import combinations


def computeConfusionMatrix(trueLabelsOneHot, predictedLabels):
    """
    Method that computes the greedy confusion matrix given the true and pred labels.
    As long as there are no clear way to compute the confusion matrix with clustering, a process that tries
    to maximize in a greedy way the assignment of real cluster / predicted cluster, has been created.

    :param trueLabelsOneHot: The true labels represented in one hot encoding.
    :param predictedLabels: The predicted labels represented in a list of assigned clusters.
    """
    trueLabels = [[i for i, x in enumerate(encoding) if x] for encoding in trueLabelsOneHot]

    nClasses = len(trueLabelsOneHot[0])
    confusionMatrix = np.zeros((nClasses, nClasses))

    for trueClasses, predClasses in zip(trueLabels, predictedLabels):
        for trueClass in trueClasses:
            for predClass in predClasses:
                confusionMatrix[trueClass, predClass] += 1

    distributions_aux = confusionMatrix.copy()
    distributions_ordered = []

    def _coherence_metric(cluster_distribution, position):
        cluster_copy = np.array(cluster_distribution.copy())
        cluster_copy *= -1
        cluster_copy[position] *= -1
        return np.sum(cluster_copy[position:])

    position_count = 0
    while len(distributions_ordered) < nClasses:
        coherence = []
        for distribution in distributions_aux:
            coherence.append(_coherence_metric(distribution, position_count))

        bestClusterIdx = np.argmax(coherence)

        distributions_ordered.append(list(distributions_aux[bestClusterIdx]))
        distributions_aux = np.delete(distributions_aux, bestClusterIdx, axis=0)

        if np.squeeze(distributions_aux).ndim == 1:
            distributions_ordered.append(list(distributions_aux[0]))
        position_count += 1

    print("   " + str(np.array(distributions_ordered, dtype=int)).replace('\n', '\n  '))


def computeFMeasure(trueLabelsOneHot, predictedLabels, verbose=False):
    """
    Method that computes the precision, recall and F-Measure given the true and pred labels.
    It computes them by counting correct and total pairs of instances satisfying conditions.

    :param trueLabelsOneHot: The true labels represented in one hot encoding.
    :param predictedLabels: The predicted labels represented in a list of assigned clusters.
    :param verbose: Indicate whether to plot the precision, recall and F-Measure computed.
    """
    trueLabels = [{i for i, x in enumerate(encoding) if x} for encoding in trueLabelsOneHot]

    pairsTrue = [(i, j) for ((i, comb0), (j, comb1)) in combinations(enumerate(trueLabels), 2)
                 if len(set(comb0).intersection(comb1)) != 0]
    pairsPred = [(i, j) for ((i, comb0), (j, comb1)) in combinations(enumerate(predictedLabels), 2)
                 if len(set(comb0).intersection(comb1)) != 0]

    correctlyIdentifiedPairs = 0
    for i, j in pairsPred:
        if not set(trueLabels[i]).isdisjoint(trueLabels[j]):
            correctlyIdentifiedPairs += 1

    precision = correctlyIdentifiedPairs / len(pairsPred)
    recall = correctlyIdentifiedPairs / len(pairsTrue)
    fMeasure = 2 * precision * recall / (precision + recall)

    if verbose:
        print(f"Precision = {precision}, Recall = {recall}, F-Measure = {fMeasure}")

    return precision, recall, fMeasure
