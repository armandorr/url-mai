import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

CIRCLE_RADIUS = 0.6
CIRCLE_CENTERS = [(-0.40, 0.40), (0.40, 0.40), (0.00, -0.40)]
CIRCLE_COLORS = [(1, 0, 0), (0, 1, 0), (0, 0, 1)]

RECT_RADIUS = 1.1
RECT_CENTERS = [(-0.40, 0.40), (0.40,  0.40), (-0.4, -0.40), (0.4,  -0.40)]
RECT_COLORS = [(1, 0, 0), (0, 1, 0), (0, 1, 1), (0, 0, 1)]


def createSyntheticDatasets(rawFolder="data/rawData/synthetic/",
                            dataFolder="data/datasets/synthetic/",
                            N_SAMPLES=1000, seed=42, plot=False):
    """
    Method that creates both the Circles and Rectangles dataset.

    :param rawFolder: Folder where to save the complete dataset created.
    :param dataFolder: Folder where to save the X and Y datasets created.
    :param N_SAMPLES: Number of samples to use in both datasets
    :param seed: The seed to be used.
    :param plot: Indicate whether to plot the constructed data.
    """
    np.random.seed(seed)
    circlesX, circlesY = _createSyntheticDataset(rawFolder + "circles.csv", dataFolder, N_SAMPLES, True)

    np.random.seed(seed)
    rectanglesX, rectanglesY = _createSyntheticDataset(rawFolder + "rectangles.csv", dataFolder, N_SAMPLES, False)

    if plot:
        plotSynthetic(circlesX, circlesY, CIRCLE_CENTERS, True, "Original circular clusters")
        plotSynthetic(rectanglesX, rectanglesY, RECT_CENTERS, False, "Original rectangular clusters")


def _createSyntheticDataset(rawPath, dataFolder, N_SAMPLES, circles=True):
    """
    Method that creates the Circles or the Rectangles dataset.

    :param rawPath: File path where to save the complete dataset created.
    :param dataFolder: Folder where to save the X and Y datasets created.
    :param N_SAMPLES: Number of samples to use in both datasets
    :param circles: Indicate whether to create the Circles or the Rectangles dataset.
    """
    centers = CIRCLE_CENTERS if circles else RECT_CENTERS
    radius = CIRCLE_RADIUS if circles else RECT_RADIUS

    results = []
    SAMPLES_IN = 0
    while SAMPLES_IN < N_SAMPLES:
        rand_x = np.random.uniform(-1, 1)
        rand_y = np.random.uniform(-1, 1)
        resultsAux = np.zeros(2 + len(centers))
        resultsAux[0], resultsAux[1] = rand_x, rand_y
        insideNum = 0
        for centerIdx, center in enumerate(centers):
            if circles:
                inside = _inCircle((rand_x, rand_y), center, radius)
            else:
                inside = _inRectangle((rand_x, rand_y), center, radius)
            resultsAux[2 + centerIdx] = inside
            insideNum += int(inside)

        if insideNum > 0:
            results.append(resultsAux)
            SAMPLES_IN += 1

    columns = ["coordX", "coordY", "First", "Second", "Third"] + (["Fourth"] if not circles else [])
    results = np.array(results)
    pd.DataFrame(results, columns=columns).to_csv(rawPath, index=False)

    name = "circles" if circles else "rectangles"
    pd.DataFrame(results[:, :2], columns=columns[:2]).to_csv(dataFolder + f"{name}X.csv", index=False)
    pd.DataFrame(results[:, 2:], columns=columns[2:]).to_csv(dataFolder + f"{name}Y.csv", index=False)

    return results[:, :2], results[:, 2:]


def plotSynthetic(arrayX, clusterAssign, centers, circles, title="", idx=0):
    """
    Method that plot the synthetic data created.

    :param arrayX: The features to plot, 2-dimensional array with shape (N, 2) with N the number of points to plot.
    :param clusterAssign: The assignment of each of the instances, must be of shape N.
    :param centers: Indicate the position of the cluster centroids to be plotted.
    :param circles: Indicate whether to plot the Circles or the Rectangles dataset.
    :param title: The title to use in the plot
    :param idx: Iterator that is used to save the image and don't overwrite the files with same title name.
    """
    xCoord = arrayX[:, 0]
    yCoord = arrayX[:, 1]

    radius = CIRCLE_RADIUS if circles else RECT_RADIUS
    colorCenters = CIRCLE_COLORS if circles else RECT_COLORS
    clusterCenters = CIRCLE_CENTERS if circles else RECT_CENTERS

    fig, ax = plt.subplots(figsize=(6, 6))
    for center, color in zip(clusterCenters, colorCenters):
        if circles:
            ax.add_patch(plt.Circle(center, radius, color=color, fill=False))
        else:
            ax.add_patch(plt.Rectangle((center[0]-radius/2, center[1]-radius/2),
                                       radius, radius, color=color, fill=False))

    colors = _predToColorCircle(clusterAssign) if circles else _predToColorRectangle(clusterAssign)

    plt.scatter(*zip(*centers), color=colorCenters)
    plt.scatter(xCoord, yCoord, s=1, color=colors)

    ax.set_facecolor('white')
    ax.spines['top'].set_color('black')
    ax.spines['bottom'].set_color('black')
    ax.spines['left'].set_color('black')
    ax.spines['right'].set_color('black')
    ax.tick_params(axis='both', color='black', labelcolor='black', length=6, width=2)
    ax.set_yticks([-1, -.5, 0, .5, 1])
    ax.set_xticks([-1, -.5, 0, .5, 1])
    ax.grid(True)

    plt.axis('equal')
    plt.ylim(-1.1, 1.1)
    plt.xlim(-1.1, 1.1)
    plt.title(title)

    plt.savefig("results/plots/"+title+str(idx)+".png", dpi=150)
    plt.show()


def _inCircle(p, c, r):
    """
    Method that indicate whether a point "p" belongs to a circumference of radius "r" and center "c".

    :param p: The point to consider.
    :param c: The center of the circumference.
    :param r: The radius of the circumference.
    """
    return float(math.sqrt((p[0] - c[0]) ** 2 + (p[1] - c[1]) ** 2) - r <= 0)


def _inRectangle(p, c, r):
    """
    Method that indicate whether a point "p" belongs to a rectangle (square) of sides "r" and center "c".

    :param p: The point to consider.
    :param c: The center of the square.
    :param r: The side length of the square.
    """
    bottomLeft = (c[0] - r / 2, c[1] - r / 2)
    upperRight = (c[0] + r / 2, c[1] + r / 2)
    return bottomLeft[0] <= p[0] <= upperRight[0] and bottomLeft[1] <= p[1] <= upperRight[1]


def _predToColorCircle(pred):
    """
    Compute the colors to use with a points in a circle that has been predicted to belong to the clusters "pred".

    :param pred: One hot representation of all the predicted points. Must be of shape (N, 3).
    """
    colors = []
    for row in pred:
        color = [0, 0, 0]
        for i in row:
            color[i] = 1
        total = sum(color)
        color = [x/total for x in color]
        colors.append(tuple(color))
    return colors


def _predToColorRectangle(pred):
    """
    Compute the colors to use with a points in a rectangle that has been predicted to belong to the clusters "pred".

    :param pred: One hot representation of all the predicted points. Must be of shape (N, 4).
    """
    colors = []
    for row in pred:
        color = (0, 0, 0)
        inside = 0
        for num in row:
            if num == 0:
                color = [color[0] + 1, color[1], color[2]]
                inside += 1
            if num == 1:
                color = [color[0], color[1] + 1, color[2]]
                inside += 1
            if num == 2:
                color = [color[0], color[1] + 1, color[2] + 1]
                inside += 1
            if num == 3:
                color = [color[0], color[1], color[2] + 1]
                inside += 1
        color = tuple(x / inside for x in color) if inside != 4 else (0, 0, 0)
        colors.append(color)
    return colors
