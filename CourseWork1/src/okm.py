import numpy as np
from scipy.spatial.distance import cdist
from numpy import linalg


class OKM:
    def __init__(self, nClusters, maxIter=100, tol=1e-4, initClusterCenters=None, seed=None, verbose=False):
        """
        Method that initializes the OKM algorithm. It basically set the parameters.

        :param nClusters: Number of clusters to use.
        :param maxIter: Maximum number of iterations to perform.
        :param tol: Tolerance to consider that the method has already converged.
        :param initClusterCenters: The initial clusters to use. If None, they will be randomly initialized.
        :param seed: The seed to use in case the initClusterCenters == None.
        :param verbose: Indicate whether to show prints and progress of the method.
        """
        self.nClusters = nClusters
        self.maxIter = maxIter
        self.tol = tol
        self.initClusterCenters = initClusterCenters
        if seed:
            np.random.seed(seed)
        self.verbose = verbose

        self.fitted = False
        self.clusters = None
        self.clusterCenters = None
        self.assignments = None

    def fit(self, X):
        """
        Method that fits the OKM algorithm with the given data. Main loop of the algorithm.

        :param X: Dataset with the features of the instances to clusterize with.
        """
        X = X.values
        self._initializeAlgorithm(X)
        self._computeAssignmentOnDataset(X, alreadyAssignment=False)
        self._deriveCoverage()

        tol = np.inf
        nIter = 0
        oldPerformance = None
        while tol > self.tol and nIter < self.maxIter:
            self._prototypeCalculation(X)
            self._computeAssignmentOnDataset(X, alreadyAssignment=True)
            self._deriveCoverage()

            newPerformance = self._objectiveFunction(X)
            if oldPerformance:
                tol = abs(oldPerformance - newPerformance)
            oldPerformance = newPerformance

            nIter += 1
            if self.verbose:
                print(f"Iteration {nIter}. Inertia: {np.round(newPerformance, 15)}")

        if self.verbose:
            print(f"Converged at iteration {nIter} by " +
                  ("strict convergence" if tol <= self.tol else "maximum iterations"))

        self.fitted = True

    def predict(self, X, newData=True):
        """
        Method that predicts the assignments of the X dataset.

        :param X: Dataset with the features of the instances to retrieve the assignments from.
        :param newData: Indicate whether the prediction will be done with newData or not.
        """
        X = X.values
        assert self.fitted, "Fit the algorithm before predicting"
        if newData:
            self.assignments = []
        self._computeAssignmentOnDataset(X, alreadyAssignment=not newData)
        return self.assignments

    def fit_predict(self, X):
        """
        Method that fits the algorithm with X dataset and then predicts the assignments of it.

        :param X: Dataset with the features of the instances to fit and predict the assignments from.
        """
        self.fit(X)
        return self.predict(X, newData=False)

    def getAssignments(self):
        """
        Getter of the assignments. The algorithm must be previously fitted.
        """
        assert self.fitted, "Fit the algorithm before getting the assignments"
        return self.assignments

    def _initializeAlgorithm(self, X):
        """
        Method that initializes the algorithm with the X values. It basically empties the arrays and
        prepare them to be fulfilled while the fitting process is carried on. It also defines the cluster
        centroids taking into account if initClusterCenters is given or not.

        :param X: Array of the features of each of the instances.
        """
        self.clusters = [[] for _ in range(self.nClusters)]
        self.clusterCenters = np.empty((self.nClusters, len(X[0])))
        self.assignments = []
        if self.initClusterCenters is not None:
            self.clusterCenters = self.initClusterCenters
        else:
            for k in range(self.nClusters):
                self.clusterCenters[k] = list(X[np.random.randint(0, len(X) - 1)])
        if self.verbose:
            print("Initialization complete")

    def _computeAssignmentOnDataset(self, X, alreadyAssignment=False):
        """
        Method that computes the assignment of instances to the actual cluster centroids.

        :param X: Array of the features of each of the instances.
        :param alreadyAssignment: Indicate whether there are already assignments for this data or not.
        """
        distances = cdist(X, self.clusterCenters)
        closestCenters = np.argsort(distances, axis=1)
        for idx, x in enumerate(X):
            newAssignment = self._computeAssignment(x, closestCenters[idx], int(alreadyAssignment) * (idx + 1))
            if not alreadyAssignment:
                self.assignments.append(newAssignment)
            else:
                self.assignments[idx] = newAssignment

    def _computeAssignment(self, x, closestCenters, oldAssignmentIndex):
        """
        Method that computes the assignment of one instance x using the closes centers.

        :param x: Instance to compute the assignments from.
        :param closestCenters: The instance x ordered closest centroids.
        :param oldAssignmentIndex: The index + 1 where the old assignment belongs in the list of assignments.
        0 indicates that there are not any old assignment.
        """
        assignment = [closestCenters[0]]
        diff = linalg.norm(x - self._imageFunction(assignment))
        for closestCenter in closestCenters[1:]:
            assignmentAux = assignment + [closestCenter]
            diffAux = linalg.norm(x - self._imageFunction(assignmentAux))
            if diffAux < diff:
                assignment = assignmentAux
                diff = diffAux
            else:
                break
        if oldAssignmentIndex == 0:
            return assignment
        oldAssignment = self.assignments[oldAssignmentIndex - 1]
        return assignment if diff <= linalg.norm(x - self._imageFunction(oldAssignment)) else oldAssignment

    def _imageFunction(self, assignment):
        """
        Method that computes the image function of an instance given its assignment. It's the center of mass
        of the cluster centroids that belong to assignments.

        :param assignment: Assignment of the instance to compute the image function from.
        """
        image = np.zeros(len(self.clusterCenters[0]))
        for centerIndex in assignment:
            image += self.clusterCenters[centerIndex]
        return image / len(assignment)

    def _deriveCoverage(self):
        """
        Method that derive the coverage using all the assignments.
        It basically indicates which instances belong to each cluster.
        """
        for k in range(self.nClusters):
            clusterK = [idx for idx, ass in enumerate(self.assignments) if k in ass]
            self.clusters[k] = clusterK

    def _prototypeCalculation(self, X):
        """
        Method that compute the new cluster centroids.

        :param X: Array of the features of each of the instances.
        """
        clusterCenters = self.clusterCenters.copy()
        for k, cluster in enumerate(self.clusters):
            first = second = 0
            for idx in cluster:
                nAssignments = len(self.assignments[idx])
                alpha = 1 / (nAssignments ** 2)
                first += alpha
                assert k in self.assignments[idx], "k should be in assignments[idx]"
                centersIndices = [a for a in self.assignments[idx] if a != k]
                mhi = nAssignments * X[idx] - np.sum(self.clusterCenters[centersIndices], axis=0)
                second += alpha * mhi

            if len(self.clusters[k]) != 0:
                clusterCenters[k] = 1 / first * second

        self.clusterCenters = clusterCenters

    def _objectiveFunction(self, X):
        """
        Method that computes the objective function using the assignments.

        :param X: Array of the features of each of the instances.
        """
        images = np.array(list(map(self._imageFunction, self.assignments)))
        return np.sum(linalg.norm(X - images, axis=1) ** 2)
