import os
import re
import numpy as np
import pandas as pd
from bs4 import Tag, BeautifulSoup
import xml.sax.saxutils as saxutils

from sklearn.preprocessing import normalize
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer

from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer, sent_tokenize

stop_words = set(stopwords.words('english'))
tokenizer = RegexpTokenizer('[\'a-zA-Z]+')
lemmatizer = WordNetLemmatizer()


def createReutersDatasets(rawFolder="data/rawData/reuters/", dataFolder="data/datasets/reuters/"):
    """
    Method that creates the Reuters-1, Reuters-2 and Reuters-3 datasets.

    :param rawFolder: Folder where to get the complete raw dataset.
    :param dataFolder: Folder where to save the X and Y of the three datasets created.
    """
    reuter1Categories = ['gold', 'ipi', 'ship', 'yen', 'dlr', 'money-fx', 'acq', 'rice', 'grain', 'crude']
    reuter2Categories = ['coffee', 'sugar', 'trade', 'rubber', 'earn', 'cpi', 'cotton', 'alum', 'bop', 'jobs']
    reuter3Categories = ['gnp', 'interest', 'veg-oil', 'oilseed', 'corn',
                         'nat-gas', 'carcass', 'livestock', 'wheat', 'soybean']
    reutersCategories = (reuter1Categories, reuter2Categories, reuter3Categories)

    reutersX = ([], [], [])
    reutersY = ([], [], [])

    totalFiles = 0
    totalArticles = 0
    totalArticlesTestTopics = 0
    totalCategories = set()

    # Traverse all the documents and save in reutersX and reutersY the corresponding body texts and categories
    for fileIdx, file in enumerate(sorted(filter(lambda x: x.endswith(".sgm"), os.listdir(rawFolder)))):
        totalFiles += 1
        data = open(f"{rawFolder}{file}", "r").read().lower()
        tree = BeautifulSoup(data, "html.parser")
        for reuter in tree.find_all("reuters"):
            totalArticles += 1
            topicsStrings = [t.string for t in reuter.topics.children]
            totalCategories.update(set(topicsStrings))
            if reuter.attrs['topics'] != "yes" or reuter.attrs['lewissplit'] != "test":
                continue
            totalArticlesTestTopics += 1

            body = _getBodyFromReuter(reuter)
            if not body:
                continue

            totalArticlesTestTopics += 1
            for categoryIdx, reuterCategories in enumerate(reutersCategories):
                categoriesIntersection = set(topicsStrings).intersection(reuterCategories)
                if len(categoriesIntersection) != 0:
                    reutersX[categoryIdx].append(body)
                    reutersY[categoryIdx].append(_toCategoryVector(categoriesIntersection, reuterCategories))

    print(f"Total files = {totalFiles}")
    print(f"Total articles = {totalArticles}")
    print(f"Total articles test = {totalArticlesTestTopics}")
    print(f"Total categories = {len(totalCategories)} = {totalCategories}")

    for reuterIdx, reuterX in enumerate(reutersX):
        print(f"Dataset number {reuterIdx + 1} contains {len(reutersX[reuterIdx])} articles.")

    for reuterIdx, reuterX in enumerate(reutersX):
        sparseMatrix = CountVectorizer(tokenizer=_tokenize, token_pattern=None, min_df=3).fit_transform(reuterX)

        lsaMatrix = TruncatedSVD(n_components=100).fit_transform(sparseMatrix)
        matrix = normalize(lsaMatrix, norm='l2')

        pd.DataFrame(matrix).to_csv(dataFolder + f"reutersX{reuterIdx + 1}.csv", index=False)
        pd.DataFrame(reutersY[reuterIdx]).to_csv(dataFolder + f"reutersY{reuterIdx + 1}.csv", index=False)


def _stripTags(text):
    """
    Method that strip the tags of a given text.
    """
    return re.sub('<[^<]+?>', '', text).strip()


def _getBodyFromReuter(reuter):
    """
    Method that retrieve the text of a reuter parsed html object.
    """
    text = [c for c in reuter.contents if (isinstance(c, Tag) and c.name == 'text')]
    if len(text) == 1:
        document_body = [c for c in text[0].contents if (isinstance(c, Tag) and c.name == 'body')]
        if len(document_body) == 1:
            document_body = _stripTags(document_body[0].text).replace('reuter\n\x03', '')
            document_body = saxutils.unescape(document_body)
            return document_body
    return None


def _tokenize(document):
    """
    Method that tokenizes and lemmatizes a document.
    """
    words = set()
    for sentence in sent_tokenize(document):
        tokens = [lemmatizer.lemmatize(t.lower()) for t in tokenizer.tokenize(sentence) if
                  t.lower() not in stop_words]
        words.update(tokens)
    return list(words)


def _toCategoryVector(categories, target_categories):
    """
    Method that transform a set of categories to a one hot vector given the target_categories.
    """
    vector = np.zeros(len(target_categories)).astype(np.float32)
    for i in range(len(target_categories)):
        if target_categories[i] in categories:
            vector[i] = 1.0
    return vector
