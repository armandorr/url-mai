# Overlapping k-Means
## Courserwork 1
Unsupervised and Reinforcement Learning (URL) - Master in Artificial Intelligence

## Author
- Armando Rodriguez Ramos

## Running script for the first time
These sections show how to create virtual environment for
our script and how to install dependencies. The first thing to do is to install Python 3.9.
1. Open folder in terminal
```bash
cd <root_folder_of_project>/
```
2. Create virtual env
```bash
python3 -m venv venv/
```
3. Open virtual env
```bash
source venv/bin/activate
```
4. Install required dependencies
```bash
pip install -r requirements.txt
```
## Execute code
1. Running the main code
- Follow the instructions on the terminal to perform the experiments.
```bash
 python3 main.py
 ```
2. Onece finished, close the virtual environment
```bash
deactivate
```
