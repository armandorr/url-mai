import numpy as np
import pandas as pd
import skfuzzy as fuzz
from src.okm import OKM
from sklearn.cluster import KMeans
from src.syntheticDatasets import plotSynthetic
from src.utils import computeFMeasure, computeConfusionMatrix


def main():
    print("Select the experiment to execute:")
    print("  [1] -Reuters")
    print("  [2] -Yeast")
    print("  [3] -Synthetic circles")
    print("  [4] -Synthetic rectangles")
    experimentNum = int(input("Choose only one number {1, 2, 3, 4}: "))

    if experimentNum not in [1, 2, 3, 4]:
        print("\nYou have selected an invalid number. Exiting.")
        return

    pathX = "data/datasets/"
    plot = 0

    if experimentNum == 1:
        print("\nSelect the Reuters subset to experiment with:")
        print("  [1] -Reuters-1")
        print("  [2] -Reuters-2")
        print("  [3] -Reuters-3")
        reuterNum = int(input("Choose one number {1, 2, 3} to select the subset: "))
        pathX += f"reuters/reutersX{reuterNum}.csv"
        nameDataset = f"subset {reuterNum} of the Reuters"
    elif experimentNum == 2:
        pathX += f"genes/yeastX.csv"
        nameDataset = "Yeast"
    else:
        syntheticName = "circles" if experimentNum == 3 else "rectangles"
        pathX += f'synthetic/{syntheticName}X.csv'
        nameDataset = f"Synthetic {syntheticName}"

    print(f"\nExperiments will be performed in the {nameDataset} dataset.\n")

    pathY = pathX.replace("X", "Y")
    datasetX = pd.read_csv(pathX)
    datasetY = pd.read_csv(pathY)

    if experimentNum in [3, 4]:
        print(f"If you select {experimentNum} clusters you will be able to see the plot of the synthetic data.")
    nClusters = int(input("Choose the number of clusters to use: "))
    if experimentNum == nClusters:
        plot = int(input("\nDo you want to see the plots of the computed clusters? Yes[1] / No[0]: ")) == 1
        plot = int(plot) * (experimentNum - 2)  # 0 == no plot, 1 == plot circles, 2 == plot rectangles

    nExperiments = int(input("\nChoose the number of experiments to do: "))

    kmeansResults = np.zeros((nExperiments, 3))
    fuzzyKmeansResults = np.zeros((nExperiments, 3))
    okmResults = np.zeros((nExperiments, 3))

    np.random.seed(42)
    seeds = np.random.randint(0, 1e5, size=nExperiments)
    for idx, seed in enumerate(seeds):
        initClusterCenters = np.empty((nClusters, len(datasetX.values[0])))

        np.random.seed(seed)
        for k in range(nClusters):
            initClusterCenters[k] = list(datasetX.values[np.random.randint(0, len(datasetX.values) - 1)])

        print(f"Iteration {idx + 1}/{len(seeds)}")
        print(f"  KMEANS ", end="")
        kmeans = KMeans(n_clusters=nClusters, n_init=1, init=initClusterCenters, random_state=seed, verbose=False)
        kmeansAssignments = kmeans.fit_predict(datasetX)
        kmeansAssignments = [[label] for label in kmeansAssignments]
        kmeansResult = computeFMeasure(datasetY.values, kmeansAssignments, verbose=True)
        kmeansResults[idx, :] = kmeansResult
        if nClusters in [3, 4] and len(datasetY.values[0]) == nClusters:
            computeConfusionMatrix(datasetY.values, kmeansAssignments)

        print(f"  FUZZY-KMEANS ", end="")
        fuzzyCenters, fuzzyMatrix, _, _, _, _, _ = fuzz.cluster.cmeans(datasetX.T, nClusters, 2,
                                                                       error=0.005, maxiter=100, seed=seed)
        fuzzyKmeansAssignments = np.argmax(fuzzyMatrix, axis=0)
        fuzzyKmeansAssignments = [[label] for label in fuzzyKmeansAssignments]
        fuzzyKmeansResult = computeFMeasure(datasetY.values, fuzzyKmeansAssignments, verbose=True)
        fuzzyKmeansResults[idx, :] = fuzzyKmeansResult
        if nClusters in [3, 4] and len(datasetY.values[0]) == nClusters:
            computeConfusionMatrix(datasetY.values, fuzzyKmeansAssignments)

        print(f"  OKM ", end="")
        okm = OKM(nClusters=nClusters, maxIter=100, initClusterCenters=initClusterCenters, verbose=False)
        okmAssignments = okm.fit_predict(datasetX)
        okmResult = computeFMeasure(datasetY.values, okmAssignments, verbose=True)
        okmResults[idx, :] = okmResult
        if nClusters in [3, 4] and len(datasetY.values[0]) == nClusters:
            computeConfusionMatrix(datasetY.values, okmAssignments)

        if plot:
            plotSynthetic(datasetX.values, kmeansAssignments, kmeans.cluster_centers_, plot == 1, "k-Means", idx)
            plotSynthetic(datasetX.values, fuzzyKmeansAssignments, fuzzyCenters, plot == 1, "fuzzy k-Means", idx)
            plotSynthetic(datasetX.values, okmAssignments, okm.clusterCenters, plot == 1, "overlapping k-Means", idx)
            print("   Plots saved at results/plots/")
        print()

    print("---------------------------------------------------------")
    print(f"KMEANS PRECISION = {np.average(kmeansResults[:, 0])}+-{np.std(kmeansResults[:, 0])}")
    print(f"FUZZY-KMEANS PRECISION = {np.average(fuzzyKmeansResults[:, 0])}+-{np.std(fuzzyKmeansResults[:, 0])}")
    print(f"OKM PRECISION = {np.average(okmResults[:, 0])}+-{np.std(okmResults[:, 0])}")
    print("---------------------------------------------------------")
    print(f"KMEANS RECALL = {np.average(kmeansResults[:, 1])}+-{np.std(kmeansResults[:, 1])}")
    print(f"FUZZY-KMEANS RECALL = {np.average(fuzzyKmeansResults[:, 1])}+-{np.std(fuzzyKmeansResults[:, 1])}")
    print(f"OKM RECALL = {np.average(okmResults[:, 1])}+-{np.std(okmResults[:, 1])}")
    print("---------------------------------------------------------")
    print(f"KMEANS F-MEASURE = {np.average(kmeansResults[:, 2])}+-{np.std(kmeansResults[:, 2])}")
    print(f"FUZZY-KMEANS F-MEASURE = {np.average(fuzzyKmeansResults[:, 2])}+-{np.std(fuzzyKmeansResults[:, 2])}")
    print(f"OKM F-MEASURE = {np.average(okmResults[:, 2])}+-{np.std(okmResults[:, 2])}")
    print("---------------------------------------------------------")

    return


if __name__ == '__main__':
    main()
